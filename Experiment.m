classdef Experiment < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess = private)
        subject
        info
        performedAt
    end
    
    properties (SetAccess = private, GetAccess = public)
        isSaved  % bool that indicates whether the object has been saved or not
    end
    
    methods (Abstract)
        run(this)
    end
   
    
    methods
        function this = Experiment()
            this.subject = input('Please enter your subject ID number: ','s');
            this.info = input('Please enter further information about the session (e.g. PVT_1, session1, ..) : ','s');
            this.performedAt = datestr(now);
            this.isSaved = false;
        end
        
        % save session info
        function save(this, savepath)
            filename = [this.subject '_-_' this.info '-' this.performedAt];
            filename = strrep(filename, ':','');
            filename = strrep(filename, ' ','-');
            if nargin == 1
                [filename,path] = uiputfile([filename '.mat']);
            elseif nargin == 2
                path = savepath;
            else
                error('too many input arguments')
            end
            
            while true
                savename = fullfile(path, filename);
                try
                    save(savename, 'this')
                    this.isSaved = true;
                    break
                catch
                    disp(['It seems you do not have the rights to write to: ' path])
                end
                [filename,path] = uiputfile([filename '.mat']);
            end
        end
        
        
        % check if object has been saved..if not provide possibility to the save data
        function delete(this)
            if ~(this.isSaved)
                answer = questdlg('The session has not been saved yet. Do you want to save it now?', ...
                    'Warning', ...
                    'Yes','No', 'Yes');
                switch answer
                    case 'Yes'
                        this.save()
                end
            end
        end
    end
end


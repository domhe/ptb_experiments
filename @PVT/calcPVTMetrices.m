function calcPVTMetrices(this)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

rt = this.rtimes; % check if copy or pointer

falseStarts       = sum(rt<100);
lapses            = sum(rt>=500);
validTrials       = sum(~isnan(rt))-falseStarts;
performanceScore  = (1-lapses-falseStarts) / (sum(~isnan(rt)));
rt                = rt(rt>=100);
rt                = sort(rt);
noTrials10        = round(numel(rt)/10);
fastest10         = mean(rt(1:noTrials10));
rt                = flip(rt);
slowest10Reci     = mean(1000./rt(1:noTrials10));

this.metrics(1,:) = {median(rt) mean(rt) fastest10 mean(1000./rt) slowest10Reci lapses lapses/validTrials falseStarts performanceScore};

end




classdef PVT < Experiment
    %PVT Summary of this class goes here
    %   Detailed explanation goes here
    
    
    properties (SetAccess = protected, GetAccess = public)
        ptb          % instance of o_ptb.PTB
        rtimes       % reaction times in ms
        metrics      % holds all standard pvt metrics
        isi          % Inter-stimuus interval in seconds
        false_start  % A False start is defined as a RT < 100 ms. „FS“ is displayed for 1 second (is part of the next ISI)
        time_out     % After 30s, "OVERRUN” is displayed for 1 second (is part of the next ISI)
        Beep         % sound2play. Visual: to wake up subject after time_out. Auditory: as Stimulus
        trigger      % triggernumbers and description
        xCenter
        yCenter
        fb_time
        color_timer
        size_timer
        color_rectangle
        size_rectangle
    end
    
    properties (Access = public)
        runtype      % PVT runs for duration_ minutes per default. Can be changed to 'trials'
        duration     % if runtype_== trials, then duration_ trials will be performed, otherwise duration_ minutes.
        method       % external: RTs are calculated by syncing the RTBox clock with the PC clock
        % internal: RTs are calculated internally by the RTBox by using
        % external triggers: photodiode (visual) or Klinke (auditory)..not
        % yet implemented
        eeg          % true: send trigger, false = default
        modality     % "visual" (default) or "auditory". If "auditory", a sound is additionally played at counter onset
    end
    
    
    methods (Access = protected)
        % calc PVT Metrics based on Basner and Dinges 2010
        calcPVTMetrices(this)
    end
    
    methods
        % constructor
        function this = PVT(ptb)
            this.ptb = ptb;
            this.rtimes = NaN(2500, 1, 'double'); % preallo more than enough memory to store the rts
            this.metrics = array2table(NaN(1,9), 'VariableNames',{'medianRT' 'meanRT' 'fastest10' 'meanReci' 'slowest10Reci' 'lapses' 'lapseProbability' 'falseStarts' 'performanceScore'});
            this.isi = [2 8];
            this.false_start = 0.1;
            this.time_out = 30;
            this.runtype = 'minutes';
            this.duration = 10;
            this.method = 'external';
            this.eeg = false;
            this.modality = 'visual';
           
            % setup Beep sound
            this.ptb.setup_audio;
            this.Beep.freq = 800;
            this.Beep.dur  = 0.4;
            this.Beep.data = o_ptb.stimuli.auditory.Sine(this.Beep.freq, this.Beep.dur);
            this.ptb.prepare_audio(this.Beep.data)
            this.ptb.schedule_audio;
            
            % setup trigger
            this.trigger.meaning = {'dummy' 'PVT_start' 'start_trial' 'start_counter' 'response' 'time_out' 'false_start'};
            this.trigger.dezimal = [1 2 4 5 6 7 8];
            
            % setup screen and get Centerpoint
            this.ptb.setup_screen;
            %clnObj = onCleanup(@() sca); % close screen when done, or in case of error
            this.xCenter = this.ptb.win_rect(3) / 2;
            this.yCenter = this.ptb.win_rect(4) / 2;
            

            % init defaults (timings are based on Basner and Dinges 2010)
            this.fb_time         = 1;           % Response time is displayed for 1 s (is part of the next ISI).
            this.color_timer     = [255 234 0]; % Color of counter
            this.size_timer      = 50;          % Text size of running timer
            this.color_rectangle = [255 0 0];   % Color rectanlge around timer
            this.size_rectangle  = [4*this.size_timer 2*this.size_timer];
        end
        
        
         % run visual PVT
        run(this)
        
        
        function displayInstruction(this)
            path = fileparts(mfilename('fullpath'));
            text = o_ptb.stimuli.visual.TextFile(fullfile(path, 'Instruction.txt'));
            text.color = 255;
            text.size  = 40;
            this.ptb.draw(text);
            this.ptb.flip();
            RTBox(inf)
            this.ptb.flip();
        end
        
        
        function plotRTs(this)
            if all(isnan(this.rtimes))
                error('No valid reaction times found. Please first run a PVT before asking for results')
            end
            figure('units','normalized','outerposition',[0 0 1 1])
            RTs = this.rtimes;
            trials = 1:sum(~isnan(RTs));
            RTs(RTs==-1) = NaN;
            plot(trials, RTs(trials), 'LineWidth',2)
            xlabel('Trials'); ylabel('RT [ms]')
            grid on
            hold on
            Median = [nanmedian(RTs),nanmedian(RTs)];
            line([trials(1), trials(end)], Median, 'LineStyle','-.', 'LineWidth', 3, 'Color', [0.4660 0.6740 0.1880])
            %yline(nanmedian(data),'-b.','Median', 'LineWidth',3);%>=matlab2018b
            hold off
            ylim([100 600])
            legend('RTs', 'Median')
        end
        
        
        % deconstructor
        function delete(~)
            sca
            % if LTP is open, close it
            try
                ppdev_mex('Close', 1);
            catch
            end
            RTBox('CloseAll');
        end
        
        %-----------------------------------------------------------
        % Setters for properties that can be changed by the user
        %-----------------------------------------------------------
        function set.runtype(this, runtype)
            if ~any(string(runtype) == ["minutes", "trials"])
                error(strcat(string(runtype), ' is no valid runtype. Only ''minutes'' and ''trials'' are accepted options.'))
            end
            this.runtype = runtype;
        end
        function set.duration(this, duration)
            if duration < 0
                error(' duration cannot be negative')
            end
            this.duration = duration;
        end
        function set.method(this, method)%
            if ~any(string(method) == ["external", "internal"])
                error(strcat(string(method), ' is no valid runtype. Only ''external'' and ''internal'' are accepted options.'))
            end
            this.method = method;
        end
        function set.eeg(this, request)
            if ~islogical(request)
                error(strcat('Only logical ''true'' (=send trigger) or ''false'' are valid inputs. Your input was:  ', string(request)))
            end
            this.eeg = request;
            if (this.eeg)
                try
                   lptwrite(1, 1) %dummy
                catch 
                    ppdev_mex('Open', 1);
                    lptwrite(1, 1) %dummy
                end
            end
        end
        function set.modality(this, modality)%
            if ~any(string(modality) == ["visual", "auditory"])
                error(strcat(string(modality), ' is no valid modality. Only ''visual'' and ''auditory'' are accepted options.'))
            end
            this.modality = modality;
        end
        
        
        %-----------------------------------------------------------
        % Getters
        %-----------------------------------------------------------
        function rtimes = get_rtimes(this)
            if all(isnan(this.rtimes))
                error('No valid reaction times found. Please first run a PVT before asking for results')
            end
            rtimes = this.rtimes(~isnan(this.rtimes));
        end
        
        function metrics = get_metrics(this)
            if all(ismissing(this.metrics))
                error('No valid reaction times found. Please first run a PVT before asking for results')
            end
            metrics = this.metrics;
        end
        
        
    end
    
end




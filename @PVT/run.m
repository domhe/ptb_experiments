function run(this)

% check modality
auditory = false;
if strcmp(this.modality, 'auditory')
    auditory = true;
end

% make sure no old rts are saved
this.rtimes = nan(numel(this.rtimes), 1);


% translate maximal trial duration to number of frames
nFrames = round(this.time_out / this.ptb.flip_interval);

% prepare print out (text for timer and rectangle)
text = cell(1, nFrames);
for pos = 1:nFrames
    txt       = sprintf('%05d', round(1000*(pos*this.ptb.flip_interval)));
    text{pos} = txt;
    %   text{pos} = o_ptb.stimuli.visual.Text(txt); % or o_ptb way..is however much slower
end
this.ptb.screen('TextFont', 'Courier');
this.ptb.screen('TextSize', this.size_timer);


% Clear / Readout the RTBox takes some time. This time will be
% saved and subtracted from the runtime.
boxClearTime = nan(numel(this.rtimes), 1); % reserve some memory..
boxReadTime  = nan(numel(this.rtimes), 1); % reserve some memory..


Priority(MaxPriority(this.ptb.win_handle))
trial = 0;
startTimePVT = GetSecs;


% Clear Box once
RTBox('clear')


% start marker + "warum up" ltp
if (this.eeg)
    lptwrite(1, this.trigger.dezimal(2)) %dummy
end

a = nan(30,1); 
while 1
    
    % start trial
    trial   = trial + 1;
    overrun = false;
    
    % clear the RTbox and measure how long this takes to correct for it
    tmp = GetSecs;
    RTBox('clear', 20); % takes ~50ms..not necessary when LIGHT but we keep it like that
    boxClearTime(trial) = GetSecs-tmp;
    
    % red rectangle only
    this.ptb.screen('FrameRect', this.color_rectangle,  [this.xCenter-this.size_rectangle(1)/2 this.yCenter-this.size_rectangle(2)/2 this.xCenter+0.5*this.size_rectangle(1) this.yCenter+0.5*this.size_rectangle(2)], 10);
    this.ptb.flip();
    if (this.eeg)
        lptwrite(1, this.trigger.dezimal(3)) %start_trial
    end
    isi = this.isi(1)-1 + (this.isi(2)-1)*rand();
    t = RTBox(isi); % wait for isi
    falseStart = ~isempty(t);
    
    
    if ~falseStart
        % start with counter: first frame with white rect for diode
        this.ptb.screen('FrameRect', this.color_rectangle,  [this.xCenter-this.size_rectangle(1)/2 this.yCenter-this.size_rectangle(2)/2 this.xCenter+0.5*this.size_rectangle(1) this.yCenter+0.5*this.size_rectangle(2)], 10);
        this.ptb.screen('FillRect', [255 255 255], [this.ptb.win_rect(3)-this.size_rectangle(1) this.yCenter-this.size_rectangle(2)/2 this.ptb.win_rect(3) this.yCenter+0.5*this.size_rectangle(2)]);
        DrawFormattedText(this.ptb.win_handle, text{1}, 'center', this.yCenter+4*this.size_timer/10, this.color_timer); %1-2ms
        
        vbl = this.ptb.flip();
        t=GetSecs;
        if auditory
            this.ptb.play_without_flip; % play sound at counter start
            a(trial) = 1000*(GetSecs-t);
        end
        
        if (this.eeg)
            lptwrite(1, this.trigger.dezimal(4)) %start_counter
        end
        startTimeTrial = vbl;
        
        for i = 2:nFrames
            
            % fill backbuffer
            this.ptb.screen('FrameRect', this.color_rectangle,  [this.xCenter-this.size_rectangle(1)/2 this.yCenter-this.size_rectangle(2)/2 this.xCenter+0.5*this.size_rectangle(1) this.yCenter+0.5*this.size_rectangle(2)], 10);
            DrawFormattedText(this.ptb.win_handle, text{i}, 'center',this.yCenter+4*this.size_timer/10, this.color_timer); %1-2ms
            %this.ptb.screen('DrawText', text{i}, xCenter+SIZE_RECTANGLE, yCenter, COLOR_TIMER); %directly calling the screen function is the fastest way to prepare the buffer <1ms
            %this.ptb.draw(text{i}); %pretty slow ~3-4ms
            
            % flip and record vbl
            vbl = this.ptb.flip(vbl + 0.5 * this.ptb.flip_interval);
            
            % stop trial if any button has been pressed
            buttonPressed = RTBox('EventsAvailable');
            if buttonPressed
                if (this.eeg)
                    lptwrite(1, this.trigger.dezimal(5)) %response
                end
                break
            end
            
            % stop trial if TIME_OUT
            if i==nFrames
                if (this.eeg)
                    lptwrite(1, this.trigger.dezimal(6)) %time_out
                end
                overrun = true;
            end
            
        end % stop trial
        
        
        % get RT
        tmp = GetSecs;
        if buttonPressed
            t = RTBox(0);
            if numel(t) > 1
                t = t(1);
            end
            this.rtimes(trial) = 1000*(t - startTimeTrial - 0.5 * this.ptb.flip_interval); %
        end
        boxReadTime(trial) = GetSecs-tmp;
        
    end %falseStart
    
    % check RT and provide FB
    if overrun
        this.rtimes(trial) = 1000*this.time_out;
        DrawFormattedText(this.ptb.win_handle, sprintf('%05d', this.time_out * 1000), 'center', this.yCenter+4*this.size_timer/10, this.color_timer); %1-2ms
        DrawFormattedText(this.ptb.win_handle, 'OVERRUN', 'center', this.yCenter+2*this.size_timer, this.color_timer);
        this.ptb.flip();
        for s = 1:3
            this.ptb.play_without_flip; % play 3 sounds to alert the subject
            WaitSecs(this.Beep.dur + 0.2)
        end
    elseif falseStart || this.rtimes(trial) < this.false_start
        lptwrite(1, this.trigger.dezimal(7)) %false_start
        this.rtimes(trial) = -1;
        DrawFormattedText(this.ptb.win_handle, '00000', 'center', this.yCenter+4*this.size_timer/10, this.color_timer); %1-2ms
        DrawFormattedText(this.ptb.win_handle, 'FALSE START', 'center',this.yCenter+2*this.size_timer, this.color_timer);
    else
        DrawFormattedText(this.ptb.win_handle, text{i}, 'center', this.yCenter+4*this.size_timer/10, this.color_timer); %1-2ms
    end
    this.ptb.screen('FrameRect', this.color_rectangle,  [this.xCenter-this.size_rectangle(1)/2 this.yCenter-this.size_rectangle(2)/2 this.xCenter+0.5*this.size_rectangle(1) this.yCenter+0.5*this.size_rectangle(2)], 10);
    this.ptb.flip();
    WaitSecs(this.fb_time);
    
    
    % stop PVT with the last response after TEST_DURATION
    if string(this.runtype) == "trials"
        if trial == this.duration
            break;
        end
    elseif GetSecs-startTimePVT-nansum(boxClearTime)-nansum(boxReadTime) > this.duration*60
        break
    end
    
end % stop PVT

Priority(0);
this.ptb.flip();

% calc PVT Metrics based on Basner and Dinges 2010 and save it in the this
calcPVTMetrices(this)

end


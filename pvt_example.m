%% clear
clear all global
restoredefaultpath
%% add the path to o_ptb
addpath('~/Desktop/Experiments/o_ptb/') % change this to where o_ptb is on your system
%% add the path to ptb_experiments
addpath(genpath('~/Desktop/Experiments/ptb_experiments'))
%% initialize the PTB
o_ptb.init_ptb('/usr/share/psychtoolbox-3'); % change this to where PTB is on your system
%% for debug
ptb_cfg = o_ptb.PTB_Config();
ptb_cfg.fullscreen = false  ;
ptb_cfg.window_scale = 0.3;
ptb_cfg.skip_sync_test = true;
ptb_cfg.hide_mouse = true;
ptb_cfg.draw_borders_sbg = true;
ptb_cfg.flip_horizontal = false;
ptb_cfg.background_color = 0;
  
% create an instance of PTB
ptb = o_ptb.PTB.get_instance(ptb_cfg);


%% PVT

% create a PVT instance
pvt = PVT(ptb);

% set PVT duration to 10 trials
pvt.runtype  = 'trials'; 
pvt.duration = 7; 

% set EEG to true = write trigger
pvt.eeg = true;

% change modality to auditory if you want to indicate the onset of the
% counter by a beep
pvt.modality = 'auditory';

%pvt.displa
pvt.displayInstruction

% run the PVT
pvt.run();

% show + print the results
pvt.get_metrics()
pvt.plotRTs()

% save the experiment
pvt.save('~/Desktop/Experiments/ptb_experiments/')

% clear
pvt.delete()
clear pvt
